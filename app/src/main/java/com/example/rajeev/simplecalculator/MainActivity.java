package com.example.rajeev.simplecalculator;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9,bP,bM,bD,bS,bE,bDot,bc;
    Boolean b= false;
    Button next1,next2,next3,next4,next5;
    TextView tv,tv1,tv2;
    String First="";
    String Second="";
    String button;
    double MFirst=0.0;
    double MSecond=0.0;
    double MTotal=0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        next1 = (Button)findViewById(R.id.nxt1);
        next2= (Button)findViewById(R.id.nxt2);
        next3 = (Button)findViewById(R.id.nxt3);
        next4= (Button)findViewById(R.id.nxt4);
        next5 = (Button)findViewById(R.id.nxt5);

        tv= (TextView)findViewById(R.id.textView);
        tv1= (TextView)findViewById(R.id.textView1);
        tv2= (TextView)findViewById(R.id.textView3);
        b0= (Button) findViewById(R.id.button0);
        b1= (Button) findViewById(R.id.button1);
        b2= (Button) findViewById(R.id.button2);
        b3= (Button) findViewById(R.id.button3);
        b4= (Button) findViewById(R.id.button4);
        b5= (Button) findViewById(R.id.button5);
        b6= (Button) findViewById(R.id.button6);
        b7= (Button) findViewById(R.id.button7);
        b8= (Button) findViewById(R.id.button8);
        b9= (Button) findViewById(R.id.button9);
        bP= (Button) findViewById(R.id.button13);
        bM= (Button) findViewById(R.id.button15);
        bD= (Button) findViewById(R.id.button16);
        bS= (Button) findViewById(R.id.button14);
        bE= (Button) findViewById(R.id.button12);
        bDot= (Button) findViewById(R.id.button11);
        bc=(Button)findViewById(R.id.buttonC);
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"0");
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"1");
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"2");

            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"5");
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+"9");
            }
        });
        bP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().trim() == "") {}
                else {
                    bDot.setClickable(true);
                    if (b == true) {
                        tv.setText("");
                        b = false;
                    }
                    First = tv.getText().toString().trim();
                    if(First=="")
                    {}
                    else {
                        button = "+";
                        tv.setText("");
                        tv1.setText(First + "\n" + "+" + "\n");
                    }
                }
            }
        });
        bS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().trim() == "") {

                } else {
                    bDot.setClickable(true);
                    if (b == true) {
                        tv.setText("");
                        b = false;
                    }
                    First = tv.getText().toString().trim();
                    if (First == "") {
                    } else {
                        button = "-";
                        tv.setText("");
                        tv1.setText(First + "\n" + "-" + "\n");
                    }
                }
            }
        });

        bM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().trim() == "") {

                } else {
                    bDot.setClickable(true);
                    if (b == true) {
                        tv.setText("");
                        b = false;
                    }
                    First = tv.getText().toString().trim();
                    if (First == "") {
                    } else {
                        button = "*";
                        tv.setText("");
                        tv1.setText(First + "\n" + "X" + "\n");
                    }
                }
            }
        });
        bD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().trim() == "") {

                } else {
                    bDot.setClickable(true);
                    if (b == true) {
                        tv.setText("");
                        b = false;
                    }
                    First = tv.getText().toString().trim();
                    if (First == "") {
                    } else {
                        button = "/";
                        tv.setText("");
                        tv1.setText(First + "\n" + "/" + "\n");
                    }
                }
            }
        });
        bDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b==true)
                {
                    tv.setText("");
                    b=false;
                }
                tv.setText(tv.getText().toString().trim()+".");
                bDot.setClickable(false);
            }
        });
        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setText("");
                tv1.setText("For any query contact me at\\n \\nrajeevjoshi91@gmail.com");
                MFirst = 0;
                MSecond = 0;
                MTotal = 0;
                First = "";
                Second = "";
            }
        });
        bE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv.getText().toString().trim() == "") {

                } else {
                    bDot.setClickable(true);

                    b = true;
                    Second = tv.getText().toString().trim();

                    if (First == "" || Second == "") {
                        Log.e(First, "Please Enter Value");
                    } else {

                        MFirst = Double.parseDouble(First);
                        MSecond = Double.parseDouble(Second);
                        if (button == "+") {
                            MTotal = MFirst + MSecond;
                            tv1.setText(First + "\n" + "+" + "\n" + Second + "\n" + "Result = " + MTotal);
                            tv.setText("" + MTotal);
                        } else if (button == "-") {
                            MTotal = MFirst - MSecond;
                            tv1.setText(First + "\n" + "-" + "\n" + Second + "\n" + "Result = " + MTotal);
                            tv.setText("" + MTotal);

                        } else if (button == "*") {
                            MTotal = MFirst * MSecond;
                            tv1.setText(First + "\n" + "X" + "\n" + Second + "\n" + "Result = " + MTotal);
                            tv.setText("" + MTotal);

                        } else if (button == "/") {
                            MTotal = MFirst / MSecond;
                            tv1.setText(First + "\n" + "/" + "\n" + Second + "\n" + "Result = " + MTotal);
                            tv.setText("" + MTotal);

                        }
                        MTotal = 0.0;
                        MFirst = 0.0;
                        MSecond = 0.0;
                        First = "";
                        Second = "";
                    }
                }
            }
        });
        next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Main2Activity.class));

            }
        });
        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new
                        Intent(MainActivity.this,Main2Activity.class));
            }
        });
        next3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Main2Activity.class));
            }
        });
        next4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Main2Activity.class));
            }
        });
        next5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Main2Activity.class));
            }
        });

    }



}
